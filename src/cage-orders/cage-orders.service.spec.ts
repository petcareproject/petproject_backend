import { Test, TestingModule } from '@nestjs/testing';
import { CageOrdersService } from './cage-orders.service';

describe('CageOrdersService', () => {
  let service: CageOrdersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CageOrdersService],
    }).compile();

    service = module.get<CageOrdersService>(CageOrdersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
