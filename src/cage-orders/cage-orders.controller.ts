import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { CageOrdersService } from './cage-orders.service';
import { CreateCageOrderDto } from './dto/create-cage-order.dto';
import { UpdateCageOrderDto } from './dto/update-cage-order.dto';

@Controller('cage-orders')
export class CageOrdersController {
  constructor(private readonly cageOrdersService: CageOrdersService) {}

  @Post()
  create(@Body() createCageOrderDto: CreateCageOrderDto) {
    return this.cageOrdersService.create(createCageOrderDto);
  }

  @Get()
  findAll() {
    return this.cageOrdersService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.cageOrdersService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateCageOrderDto: UpdateCageOrderDto,
  ) {
    return this.cageOrdersService.update(+id, updateCageOrderDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.cageOrdersService.remove(+id);
  }
}
