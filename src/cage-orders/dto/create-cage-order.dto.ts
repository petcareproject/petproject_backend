import { IsNotEmpty } from 'class-validator';

export class CreateCageOrderDto {
  @IsNotEmpty()
  cageOrderStatus: string;

  @IsNotEmpty()
  cageId: number;

  @IsNotEmpty()
  bgbId: number;

  @IsNotEmpty()
  roomId: number;

  @IsNotEmpty()
  petId: number;
}
