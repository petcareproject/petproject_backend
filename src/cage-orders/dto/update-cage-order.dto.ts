import { PartialType } from '@nestjs/mapped-types';
import { CreateCageOrderDto } from './create-cage-order.dto';

export class UpdateCageOrderDto extends PartialType(CreateCageOrderDto) {}
