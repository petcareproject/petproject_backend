import { Module } from '@nestjs/common';
import { CageOrdersService } from './cage-orders.service';
import { CageOrdersController } from './cage-orders.controller';
import { CageOrder } from './entities/cage-order.entity';
import { Cage } from 'src/cages/entities/cage.entity';
import { Pet } from 'src/pets/entities/pet.entity';
import { Room } from 'src/rooms/entities/room.entity';
import { BGbooking } from 'src/bg-booking/entities/bg-booking.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([CageOrder, Cage, Pet, Room, BGbooking])],
  controllers: [CageOrdersController],
  providers: [CageOrdersService],
})
export class CageOrdersModule {}
