import { BGbooking } from 'src/bg-booking/entities/bg-booking.entity';
import { Cage } from 'src/cages/entities/cage.entity';
import { Pet } from 'src/pets/entities/pet.entity';
import { Room } from 'src/rooms/entities/room.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class CageOrder {
  @PrimaryGeneratedColumn({ comment: 'รหัสกรงของorder' })
  cageOrderId: number;

  @Column({ comment: 'สถานะกรง' })
  cageOrderStatus: string;

  @ManyToOne(() => Cage, (cage) => cage.cageOrders)
  @JoinColumn({ name: 'cageId', referencedColumnName: 'cageId' })
  cage: Cage;

  @ManyToOne(() => BGbooking, (bgbooking) => bgbooking.cageOrder)
  @JoinColumn({ name: 'bgbId', referencedColumnName: 'bgbId' })
  bgbooking: BGbooking;

  @ManyToOne(() => Room, (room) => room.cageOrder)
  @JoinColumn({ name: 'roomId', referencedColumnName: 'roomId' })
  room: Room;

  @ManyToOne(() => Pet, (pet) => pet.cageOrders)
  @JoinColumn({ name: 'petId', referencedColumnName: 'petId' })
  pet: Pet;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;
}
