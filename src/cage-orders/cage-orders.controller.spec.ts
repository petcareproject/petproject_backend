import { Test, TestingModule } from '@nestjs/testing';
import { CageOrdersController } from './cage-orders.controller';
import { CageOrdersService } from './cage-orders.service';

describe('CageOrdersController', () => {
  let controller: CageOrdersController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CageOrdersController],
      providers: [CageOrdersService],
    }).compile();

    controller = module.get<CageOrdersController>(CageOrdersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
