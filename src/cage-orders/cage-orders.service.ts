import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateCageOrderDto } from './dto/create-cage-order.dto';
import { UpdateCageOrderDto } from './dto/update-cage-order.dto';
import { CageOrder } from './entities/cage-order.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Cage } from 'src/cages/entities/cage.entity';
import { Repository } from 'typeorm';
import { Room } from 'src/rooms/entities/room.entity';
import { Pet } from 'src/pets/entities/pet.entity';
import { BGbooking } from 'src/bg-booking/entities/bg-booking.entity';

@Injectable()
export class CageOrdersService {
  constructor(
    @InjectRepository(CageOrder)
    private cageordersRepository: Repository<CageOrder>,
    @InjectRepository(Cage)
    private cagesRepository: Repository<Cage>,
    @InjectRepository(Room)
    private roomsRepository: Repository<Room>,
    @InjectRepository(Pet)
    private petsRepository: Repository<Pet>,
    @InjectRepository(BGbooking)
    private bgbookingsRepository: Repository<BGbooking>,
  ) {}
  async create(createCageOrderDto: CreateCageOrderDto) {
    const cage = await this.cagesRepository.findOne({
      where: {
        cageId: createCageOrderDto.cageId,
      },
    });
    const room = await this.roomsRepository.findOne({
      where: {
        roomId: createCageOrderDto.roomId,
      },
    });
    const pet = await this.petsRepository.findOne({
      where: {
        petId: createCageOrderDto.petId,
      },
    });
    const bgbooking = await this.bgbookingsRepository.findOne({
      where: {
        bgbId: createCageOrderDto.bgbId,
      },
    });

    const cageOrder = new CageOrder();
    cageOrder.cageOrderStatus = createCageOrderDto.cageOrderStatus;
    cageOrder.cage = cage;
    cageOrder.room = room;
    cageOrder.pet = pet;
    cageOrder.bgbooking = bgbooking;
    await this.cageordersRepository.save(cageOrder);
    return this.cageordersRepository.findOne({
      where: { cageOrderId: cageOrder.cageOrderId },
      relations: ['cage', 'room', 'pet', 'bgbooking'],
    });
  }

  findAll() {
    return this.cageordersRepository.find({
      relations: ['cage', 'room', 'pet', 'bgbooking'],
    });
  }

  async findOne(cageOrderId: number) {
    const cageOrder = await this.cageordersRepository.findOne({
      where: { cageOrderId: cageOrderId },
      relations: ['cage', 'room', 'pet', 'bgbooking'],
    });
    if (!cageOrder) {
      throw new NotFoundException();
    }
    return cageOrder;
  }

  async update(cageOrderId: number, updateCageOrderDto: UpdateCageOrderDto) {
    const cageOrder = await this.cageordersRepository.findOne({
      where: { cageOrderId: cageOrderId },
    });
    if (!cageOrder) {
      throw new NotFoundException();
    }
    const updateCageOrder = Object.assign(cageOrder, updateCageOrderDto);
    return this.cageordersRepository.save(updateCageOrder);
  }

  async remove(cageOrderId: number) {
    const cageOrder = await this.cageordersRepository.findOne({
      where: { cageOrderId: cageOrderId },
    });
    if (!cageOrder) {
      throw new ConflictException();
    }
    return this.cageordersRepository.remove(cageOrder);
  }
}
