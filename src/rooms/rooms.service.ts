import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateRoomDto } from './dto/create-room.dto';
// import { UpdateRoomDto } from './dto/update-room.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Room } from './entities/room.entity';
import { Repository } from 'typeorm';

@Injectable()
export class RoomsService {
  constructor(
    @InjectRepository(Room)
    private roomRepository: Repository<Room>,
  ) {}
  create(createRoomDto: CreateRoomDto) {
    return this.roomRepository.save(createRoomDto);
  }

  findAll() {
    return this.roomRepository.find();
  }

  async findOne(roomId: number) {
    const room = await this.roomRepository.findOne({
      where: { roomId: roomId },
    });
    if (!room) {
      throw new NotFoundException();
    }
    return room;
  }

  // async update(roomId: number, updateRoomDto: UpdateRoomDto) {
  //   const room = await this.roomRepository.findOneBy({
  //     roomId: roomId,
  //   });
  //   if (!room) {
  //     throw new NotFoundException();
  //   }
  //   const updateRoom = Object.assign(room, updateRoomDto);
  //   return this.roomRepository.save(updateRoom);
  // }

  async remove(roomId: number) {
    const room = await this.roomRepository.findOneBy({ roomId: roomId });
    if (!room) {
      throw new ConflictException();
    }
    return this.roomRepository.remove(room);
  }
}
