import { Module } from '@nestjs/common';
import { RoomsService } from './rooms.service';
import { RoomsController } from './rooms.controller';
import { Room } from './entities/room.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Cage } from 'src/cages/entities/cage.entity';
import { CageOrder } from 'src/cage-orders/entities/cage-order.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Room, Cage, CageOrder])],
  controllers: [RoomsController],
  providers: [RoomsService],
})
export class RoomsModule {}
