import { CageOrder } from 'src/cage-orders/entities/cage-order.entity';
import { Cage } from 'src/cages/entities/cage.entity';
import {
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Room {
  @PrimaryGeneratedColumn({ comment: 'รหัสห้อง' })
  roomId: number;

  @OneToMany(() => Cage, (cages) => cages.room)
  cages: Cage[];

  @OneToMany(() => CageOrder, (cageOrder) => cageOrder.room)
  cageOrder: CageOrder[];

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;
}
