import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateServiceDto } from './dto/create-service.dto';
import { UpdateServiceDto } from './dto/update-service.dto';
import { Service } from './entities/service.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ServicesService {
  constructor(
    @InjectRepository(Service)
    private serviceRepository: Repository<Service>,
  ) {}
  create(createServiceDto: CreateServiceDto) {
    return this.serviceRepository.save(createServiceDto);
  }

  findAll() {
    return this.serviceRepository.find();
  }

  async findOne(serviceId: number) {
    const service = await this.serviceRepository.findOne({
      where: { serviceId: serviceId },
    });
    if (!service) {
      throw new NotFoundException();
    }
    return service;
  }

  async update(serviceId: number, updateServiceDto: UpdateServiceDto) {
    const service = await this.serviceRepository.findOneBy({
      serviceId: serviceId,
    });
    if (!service) {
      throw new NotFoundException();
    }
    const updateService = Object.assign(service, updateServiceDto);
    return this.serviceRepository.save(updateService);
  }

  async remove(serviceId: number) {
    const service = await this.serviceRepository.findOneBy({
      serviceId: serviceId,
    });
    if (!service) {
      throw new NotFoundException();
    }
    return this.serviceRepository.remove(service);
  }
}
