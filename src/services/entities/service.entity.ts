import { Option } from 'src/options/entities/option.entity';
import { ServiceOrder } from 'src/service-order/entities/service-order.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Service {
  @PrimaryGeneratedColumn()
  serviceId: number;

  @Column()
  serviceName: string;

  @OneToMany(() => Option, (options) => options.service)
  options: Option[];

  @OneToMany(() => ServiceOrder, (serviceOrders) => serviceOrders.service)
  serviceOrders: ServiceOrder[];

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;
}
