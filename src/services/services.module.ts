import { Module } from '@nestjs/common';
import { ServicesService } from './services.service';
import { ServicesController } from './services.controller';
import { Option } from 'src/options/entities/option.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServiceOrder } from 'src/service-order/entities/service-order.entity';
import { Service } from './entities/service.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ServiceOrder, Service, Option])],
  controllers: [ServicesController],
  providers: [ServicesService],
})
export class ServicesModule {}
