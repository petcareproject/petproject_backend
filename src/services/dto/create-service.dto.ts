import { IsNotEmpty, MinLength } from 'class-validator';

export class CreateServiceDto {
  @IsNotEmpty()
  @MinLength(4, { message: 'ขั้นต่ำของ username คือ 4 ตัวอักษร' })
  serviceName: string;
}
