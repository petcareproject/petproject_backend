import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ServiceOptionOrdersService } from './service-option-orders.service';
import { CreateServiceOptionOrderDto } from './dto/create-service-option-order.dto';
import { UpdateServiceOptionOrderDto } from './dto/update-service-option-order.dto';

@Controller('serviceoptionorders')
export class ServiceOptionOrdersController {
  constructor(
    private readonly serviceOptionOrdersService: ServiceOptionOrdersService,
  ) {}

  @Post()
  create(@Body() createServiceOptionOrderDto: CreateServiceOptionOrderDto) {
    return this.serviceOptionOrdersService.create(createServiceOptionOrderDto);
  }

  @Get()
  findAll() {
    return this.serviceOptionOrdersService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.serviceOptionOrdersService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateServiceOptionOrderDto: UpdateServiceOptionOrderDto,
  ) {
    return this.serviceOptionOrdersService.update(
      +id,
      updateServiceOptionOrderDto,
    );
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.serviceOptionOrdersService.remove(+id);
  }
}
