import { Option } from 'src/options/entities/option.entity';
import { ServiceOrder } from 'src/service-order/entities/service-order.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class ServiceOptionOrder {
  @PrimaryGeneratedColumn()
  serviceOptionId: number;

  @Column()
  optionName: string;

  @Column({ default: 0 })
  optionPrice: number;

  @ManyToOne(() => Option, (option) => option.serviceOptionOrders)
  @JoinColumn({ name: 'optionId', referencedColumnName: 'optionId' })
  option: Option;

  @ManyToOne(
    () => ServiceOrder,
    (serviceOrder) => serviceOrder.serviceOptionOrders,
  )
  @JoinColumn({
    name: 'serviceOrderId',
    referencedColumnName: 'serviceOrderId',
  })
  serviceOrder: ServiceOrder;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;
}
