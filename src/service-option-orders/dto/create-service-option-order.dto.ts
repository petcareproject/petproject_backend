import { IsNotEmpty, IsPositive, MinLength } from 'class-validator';

export class CreateServiceOptionOrderDto {
  @IsNotEmpty()
  @MinLength(4, { message: 'ขั้นต่ำของ username คือ 4 ตัวอักษร' })
  optionName: string;

  @IsPositive()
  optionPrice: number;

  @IsNotEmpty()
  optionId: number;

  @IsNotEmpty()
  serviceOrderId: number;
}
