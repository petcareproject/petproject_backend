import { PartialType } from '@nestjs/mapped-types';
import { CreateServiceOptionOrderDto } from './create-service-option-order.dto';

export class UpdateServiceOptionOrderDto extends PartialType(
  CreateServiceOptionOrderDto,
) {}
