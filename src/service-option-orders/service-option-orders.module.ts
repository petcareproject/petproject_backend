import { Module } from '@nestjs/common';
import { ServiceOptionOrdersService } from './service-option-orders.service';
import { ServiceOptionOrdersController } from './service-option-orders.controller';
import { ServiceOptionOrder } from './entities/service-option-order.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServiceOrder } from 'src/service-order/entities/service-order.entity';
import { Option } from 'src/options/entities/option.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([ServiceOptionOrder, ServiceOrder, Option]),
  ],
  controllers: [ServiceOptionOrdersController],
  providers: [ServiceOptionOrdersService],
})
export class ServiceOptionOrdersModule {}
