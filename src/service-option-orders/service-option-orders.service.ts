import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateServiceOptionOrderDto } from './dto/create-service-option-order.dto';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ServiceOptionOrder } from './entities/service-option-order.entity';
import { ServiceOrder } from 'src/service-order/entities/service-order.entity';
import { UpdateServiceOptionOrderDto } from './dto/update-service-option-order.dto';
import { Option } from 'src/options/entities/option.entity';

@Injectable()
export class ServiceOptionOrdersService {
  constructor(
    @InjectRepository(Option)
    private optionRepository: Repository<Option>,
    @InjectRepository(ServiceOrder)
    private serviceOrderRepository: Repository<ServiceOrder>,
    @InjectRepository(ServiceOptionOrder)
    private serviceOptionOrderRepository: Repository<ServiceOptionOrder>,
  ) {}
  async create(createServiceOptionOrderDto: CreateServiceOptionOrderDto) {
    const option = await this.optionRepository.findOne({
      where: {
        optionId: createServiceOptionOrderDto.optionId,
      },
    });
    const serviceOrder = await this.serviceOrderRepository.findOne({
      where: {
        serviceOrderId: createServiceOptionOrderDto.serviceOrderId,
      },
    });

    const sot = new ServiceOptionOrder();
    sot.optionName = createServiceOptionOrderDto.optionName;
    sot.optionPrice = createServiceOptionOrderDto.optionPrice;
    sot.option = option;
    sot.serviceOrder = serviceOrder;
    await this.serviceOptionOrderRepository.save(sot);
    return this.serviceOptionOrderRepository.findOne({
      where: { serviceOptionId: sot.serviceOptionId },
      relations: ['option, serviceOrder'],
    });
  }

  async findByServiceOrderId(serviceOrderId: number) {
    return await this.serviceOptionOrderRepository.find({
      where: { serviceOrder: { serviceOrderId } },
    });
  }

  async findAll() {
    return await this.serviceOptionOrderRepository.find({
      relations: {
        option: true,
        serviceOrder: true,
      },
    });
  }

  async findOne(serviceOptionId: number) {
    const sot = await this.serviceOptionOrderRepository.findOne({
      where: { serviceOptionId: serviceOptionId },
      relations: {
        option: true,
        serviceOrder: true,
      },
    });
    if (!sot) {
      throw new NotFoundException();
    }
    return sot;
  }

  async update(
    serviceOptionId: number,
    updateServiceOptionOrderDto: UpdateServiceOptionOrderDto,
  ) {
    const sot = await this.serviceOptionOrderRepository.findOne({
      where: { serviceOptionId: serviceOptionId },
    });
    if (!sot) {
      throw new NotFoundException();
    }
    const updatesot = Object.assign(sot, updateServiceOptionOrderDto);
    return this.serviceOptionOrderRepository.save(updatesot);
  }

  async remove(serviceOptionId: number) {
    const sot = await this.serviceOptionOrderRepository.findOneBy({
      serviceOptionId: serviceOptionId,
    });
    if (!sot) {
      throw new ConflictException();
    }
    return this.serviceOptionOrderRepository.remove(sot);
  }
}
