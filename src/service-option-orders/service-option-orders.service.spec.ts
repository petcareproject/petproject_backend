import { Test, TestingModule } from '@nestjs/testing';
import { ServiceOptionOrdersService } from './service-option-orders.service';

describe('ServiceOptionOrdersService', () => {
  let service: ServiceOptionOrdersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ServiceOptionOrdersService],
    }).compile();

    service = module.get<ServiceOptionOrdersService>(
      ServiceOptionOrdersService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
