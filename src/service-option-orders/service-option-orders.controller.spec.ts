import { Test, TestingModule } from '@nestjs/testing';
import { ServiceOptionOrdersController } from './service-option-orders.controller';
import { ServiceOptionOrdersService } from './service-option-orders.service';

describe('ServiceOptionOrdersController', () => {
  let controller: ServiceOptionOrdersController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ServiceOptionOrdersController],
      providers: [ServiceOptionOrdersService],
    }).compile();

    controller = module.get<ServiceOptionOrdersController>(
      ServiceOptionOrdersController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
