import { Module } from '@nestjs/common';
import { ServiceOrderService } from './service-order.service';
import { ServiceOrderController } from './service-order.controller';
import { ServiceOrder } from './entities/service-order.entity';
import { BGbooking } from 'src/bg-booking/entities/bg-booking.entity';
import { ServiceOptionOrder } from 'src/service-option-orders/entities/service-option-order.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Service } from 'src/services/entities/service.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ServiceOrder,
      BGbooking,
      ServiceOptionOrder,
      Service,
    ]),
  ],
  controllers: [ServiceOrderController],
  providers: [ServiceOrderService],
})
export class ServiceOrderModule {}
