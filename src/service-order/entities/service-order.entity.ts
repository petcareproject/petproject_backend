import { BGbooking } from 'src/bg-booking/entities/bg-booking.entity';
import { ServiceOptionOrder } from 'src/service-option-orders/entities/service-option-order.entity';
import { Service } from 'src/services/entities/service.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class ServiceOrder {
  @PrimaryGeneratedColumn()
  serviceOrderId: number;

  @Column()
  serviceName: string;

  @Column({ default: 0 })
  totalPrice: number;

  @Column()
  note: string;

  @ManyToOne(() => Service, (service) => service.serviceOrders)
  @JoinColumn({ name: 'serviceId', referencedColumnName: 'serviceId' })
  service: Service;

  @OneToMany(
    () => ServiceOptionOrder,
    (serviceOptionOrders) => serviceOptionOrders.serviceOrder,
    { cascade: true },
  )
  serviceOptionOrders: ServiceOptionOrder[];

  @ManyToOne(() => BGbooking, (bgbooking) => bgbooking.serviceOrders)
  @JoinColumn({ name: 'bgbId', referencedColumnName: 'bgbId' })
  bgbooking: BGbooking;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;
}
