import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateServiceOrderDto } from './dto/create-service-order.dto';
import { UpdateServiceOrderDto } from './dto/update-service-order.dto';
import { ServiceOrder } from './entities/service-order.entity';
import { BGbooking } from 'src/bg-booking/entities/bg-booking.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Service } from 'src/services/entities/service.entity';

@Injectable()
export class ServiceOrderService {
  constructor(
    @InjectRepository(ServiceOrder)
    private serviceOrderRepository: Repository<ServiceOrder>,
    @InjectRepository(Service)
    private serviceRepository: Repository<Service>,
    @InjectRepository(BGbooking)
    private bgbookingOrderRepository: Repository<BGbooking>,
  ) {}
  async create(createServiceOrderDto: CreateServiceOrderDto) {
    const service = await this.serviceRepository.findOne({
      where: {
        serviceId: createServiceOrderDto.serviceId,
      },
    });
    const bgbooking = await this.bgbookingOrderRepository.findOne({
      where: {
        bgbId: createServiceOrderDto.bgbId,
      },
    });

    const serviceOrder = new ServiceOrder();
    serviceOrder.serviceName = createServiceOrderDto.serviceName;
    serviceOrder.totalPrice = createServiceOrderDto.totalPrice;
    serviceOrder.note = createServiceOrderDto.note;
    serviceOrder.service = service;
    serviceOrder.bgbooking = bgbooking;
    await this.serviceOrderRepository.save(serviceOrder);
    return this.serviceOrderRepository.findOne({
      where: { serviceOrderId: serviceOrder.serviceOrderId },
      relations: ['service', 'bgbooking'],
    });
  }
  x;
  async findAll() {
    return this.serviceOrderRepository.find({
      relations: ['service', 'bgbooking'],
    });
  }

  async findOne(id: number) {
    const serviceOrder = await this.serviceOrderRepository.findOne({
      where: { serviceOrderId: id },
      relations: ['service', 'bgbooking'],
    });
    return serviceOrder;
  }

  async update(id: number, updateServiceOrderDto: UpdateServiceOrderDto) {
    const serviceOrder = await this.serviceOrderRepository.findOne({
      where: { serviceOrderId: id },
      relations: ['service', 'bgbooking'],
    });
    if (!serviceOrder) {
      throw new NotFoundException();
    }
    const updateServiceOrder = Object.assign(
      serviceOrder,
      updateServiceOrderDto,
    );
    return this.serviceOrderRepository.save(updateServiceOrder);
  }

  async remove(id: number) {
    const serviceOrder = await this.serviceOrderRepository.findOne({
      where: { serviceOrderId: id },
      relations: ['service', 'bgbooking'],
    });
    return this.serviceOrderRepository.remove(serviceOrder);
  }
  async findByBookingId(bgbId: number) {
    return await this.serviceOrderRepository.find({
      where: { bgbooking: { bgbId } },
    });
  }
}
