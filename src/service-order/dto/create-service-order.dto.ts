import { IsNotEmpty } from 'class-validator';

export class CreateServiceOrderDto {
  @IsNotEmpty()
  serviceName: string;

  totalPrice: number;

  note: string;

  serviceId: number;

  bgbId: number;
}
