import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { UpdateOptionDto } from './dto/update-option.dto';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Service } from 'src/services/entities/service.entity';
import { Option } from './entities/option.entity';
import { CreateOptionDto } from './dto/create-option.dto';

@Injectable()
export class OptionsService {
  constructor(
    @InjectRepository(Option)
    private optionRepository: Repository<Option>,
    @InjectRepository(Service)
    private serviceRepository: Repository<Service>,
  ) {}
  async create(createOptionDto: CreateOptionDto) {
    const service = await this.serviceRepository.findOne({
      where: {
        serviceId: createOptionDto.serviceId,
      },
    });

    const option = new Option();
    option.optionName = createOptionDto.optionName;
    option.service = service;
    await this.optionRepository.save(option);
    return this.optionRepository.findOne({
      where: { optionId: option.optionId },
      relations: ['service'],
    });
  }
  async findAll() {
    return this.optionRepository.find({
      relations: ['service'],
    });
  }

  async findOne(optionId: number) {
    const option = await this.optionRepository.findOne({
      where: { optionId: optionId },
      relations: ['service'],
    });
    if (!option) {
      throw new NotFoundException();
    }
    return option;
  }

  async update(optionId: number, updateOptionDto: UpdateOptionDto) {
    const option = await this.optionRepository.findOne({
      where: { optionId: optionId },
    });
    if (!option) {
      throw new NotFoundException();
    }
    const updateOption = Object.assign(option, updateOptionDto);
    return this.optionRepository.save(updateOption);
  }

  async remove(optionId: number) {
    const option = await this.optionRepository.findOneBy({
      optionId: optionId,
    });
    if (!option) {
      throw new ConflictException();
    }
    return this.optionRepository.remove(option);
  }

  async findByServiceId(serviceId: number) {
    return await this.optionRepository.find({
      where: { service: { serviceId } },
    });
  }
}
