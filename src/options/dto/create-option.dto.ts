import { IsNotEmpty, MinLength } from 'class-validator';

export class CreateOptionDto {
  @IsNotEmpty()
  @MinLength(4, { message: 'ขั้นต่ำของ username คือ 4 ตัวอักษร' })
  optionName: string;

  @IsNotEmpty()
  serviceId: number;
}
