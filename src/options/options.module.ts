import { Module } from '@nestjs/common';
import { OptionsService } from './options.service';
import { OptionsController } from './options.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Service } from 'src/services/entities/service.entity';
import { Option } from './entities/option.entity';
import { ServiceOptionOrder } from 'src/service-option-orders/entities/service-option-order.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Option, Service, ServiceOptionOrder])],
  controllers: [OptionsController],
  providers: [OptionsService],
})
export class OptionsModule {}
