import { ServiceOptionOrder } from 'src/service-option-orders/entities/service-option-order.entity';
import { Service } from 'src/services/entities/service.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Option {
  @PrimaryGeneratedColumn()
  optionId: number;

  @Column()
  optionName: string;

  @ManyToOne(() => Service, (service) => service.options)
  @JoinColumn({ name: 'serviceId', referencedColumnName: 'serviceId' })
  service: Service;

  @OneToMany(
    () => ServiceOptionOrder,
    (serviceOptionOrders) => serviceOptionOrders.option,
  )
  serviceOptionOrders: ServiceOptionOrder[];

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;
}
