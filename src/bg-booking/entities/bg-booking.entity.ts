import { CageOrder } from 'src/cage-orders/entities/cage-order.entity';
import { Customer } from 'src/customers/entities/customer.entity';
import { Pet } from 'src/pets/entities/pet.entity';
import { ServiceOrder } from 'src/service-order/entities/service-order.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class BGbooking {
  @PrimaryGeneratedColumn({ comment: 'รหัสการจอง' })
  bgbId: number;

  @Column({ comment: 'วันที่จอง' })
  bgbBookingDate: string;

  @Column({ comment: 'เวลาที่จอง' })
  bgbBookingTime: string;

  @Column({ comment: 'สถานะของการจอง' })
  bgbStatus: string;

  @Column({ comment: 'ค่ามัดจํา' })
  bgbDepositPrice: number;

  @Column({ comment: 'นํ้าหนักสัตว์เลี้ยง' })
  bgbPetWeight: number;

  @Column({ comment: 'ค่าบริการ' })
  bgbServicePrice: number;

  @Column({ comment: 'ราคารวม' })
  bgbTotalPrice: number;

  @Column({ comment: 'สถานะค่ามัดจํา' })
  bgbDepPayStatus: string;

  @Column({ comment: 'ช่องทางการชําระ' })
  bgbDepPayMethod: string;

  @Column({ comment: 'สถานะค่าเงินจํานวนเต็ม' })
  bgbTotalPayStatus: string;

  @Column({ comment: 'ช่องทางการชําระ' })
  bgbTotalPayMethod: string;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @ManyToOne(() => Customer, (customer) => customer.bgbookings)
  @JoinColumn({ name: 'cusId', referencedColumnName: 'cusId' })
  customer: Customer;

  @OneToMany(() => ServiceOrder, (serviceOrders) => serviceOrders.bgbooking, {
    cascade: true,
  })
  serviceOrders: ServiceOrder[];

  @ManyToOne(() => Pet, (pet) => pet.bgbookings)
  @JoinColumn({ name: 'petId', referencedColumnName: 'petId' })
  pet: Pet;

  @OneToMany(() => CageOrder, (cageOrder) => cageOrder.bgbooking, {
    cascade: true,
  })
  @JoinColumn({ name: 'cageOrderId', referencedColumnName: 'cageOrderId' })
  cageOrder: CageOrder[];
}
