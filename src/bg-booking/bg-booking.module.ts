import { Module } from '@nestjs/common';
import { BgBookingService } from './bg-booking.service';
import { BgBookingController } from './bg-booking.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BGbooking } from './entities/bg-booking.entity';
import { Customer } from 'src/customers/entities/customer.entity';
import { ServiceOrder } from 'src/service-order/entities/service-order.entity';
import { Pet } from 'src/pets/entities/pet.entity';
import { CageOrder } from 'src/cage-orders/entities/cage-order.entity';
import { ServiceOptionOrder } from 'src/service-option-orders/entities/service-option-order.entity';
import { Service } from 'src/services/entities/service.entity';
import { Option } from 'src/options/entities/option.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      BGbooking,
      Customer,
      ServiceOrder,
      Pet,
      CageOrder,
      ServiceOptionOrder,
      Option,
      Service,
    ]),
  ],
  controllers: [BgBookingController],
  providers: [BgBookingService],
})
export class BgBookingModule {}
