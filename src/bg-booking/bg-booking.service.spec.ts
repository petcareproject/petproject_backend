import { Test, TestingModule } from '@nestjs/testing';
import { BgBookingService } from './bg-booking.service';

describe('BgBookingService', () => {
  let service: BgBookingService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BgBookingService],
    }).compile();

    service = module.get<BgBookingService>(BgBookingService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
