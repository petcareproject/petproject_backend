import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateBgBookingDto } from './dto/create-bg-booking.dto';
import { UpdateBgBookingDto } from './dto/update-bg-booking.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { BGbooking } from './entities/bg-booking.entity';
import { Like, Repository } from 'typeorm';
import { Pet } from 'src/pets/entities/pet.entity';
import { Customer } from 'src/customers/entities/customer.entity';
import { ServiceOrder } from 'src/service-order/entities/service-order.entity';
import { ServiceOptionOrder } from 'src/service-option-orders/entities/service-option-order.entity';
import { Option } from 'src/options/entities/option.entity';
import { Service } from 'src/services/entities/service.entity';
import { CageOrder } from 'src/cage-orders/entities/cage-order.entity';

@Injectable()
export class BgBookingService {
  constructor(
    @InjectRepository(BGbooking)
    private bgBookingRepository: Repository<BGbooking>,
    @InjectRepository(Pet)
    private petRepository: Repository<Pet>,
    @InjectRepository(Customer)
    private customerRepository: Repository<Customer>,
    @InjectRepository(Option)
    private optionRepository: Repository<Option>,
    @InjectRepository(Service)
    private serviceRepository: Repository<Service>,
    @InjectRepository(ServiceOrder)
    private serviceOrderRepository: Repository<ServiceOrder>,
    @InjectRepository(ServiceOptionOrder)
    private serviceOptionOrderRepository: Repository<ServiceOptionOrder>,
    @InjectRepository(CageOrder)
    private cageOrderRepository: Repository<CageOrder>,
  ) {}
  async create(createBgBookingDto: CreateBgBookingDto) {
    const existingBooking = await this.bgBookingRepository.findOne({
      where: {
        bgbBookingDate: createBgBookingDto.bgbBookingDate,
        bgbBookingTime: createBgBookingDto.bgbBookingTime,
        pet: {
          petId: createBgBookingDto.petId,
        },
      },
    });

    if (existingBooking) {
      throw new ConflictException('This booking already exists');
    }
    const pet = await this.petRepository.findOne({
      where: {
        petId: createBgBookingDto.petId,
      },
    });
    const customer = await this.customerRepository.findOne({
      where: {
        cusId: createBgBookingDto.cusId,
      },
    });
    const bgBooking = new BGbooking();
    bgBooking.bgbBookingDate = createBgBookingDto.bgbBookingDate;
    bgBooking.bgbBookingTime = createBgBookingDto.bgbBookingTime;
    bgBooking.bgbDepositPrice = 200;
    bgBooking.bgbStatus = createBgBookingDto.bgbStatus;
    bgBooking.bgbPetWeight = createBgBookingDto.bgbPetWeight;
    bgBooking.bgbServicePrice = createBgBookingDto.bgbServicePrice;
    bgBooking.bgbTotalPrice = createBgBookingDto.bgbTotalPrice;
    bgBooking.bgbDepPayStatus = createBgBookingDto.bgbDepPayStatus;
    bgBooking.bgbDepPayMethod = createBgBookingDto.bgbDepPayMethod;
    bgBooking.bgbTotalPayStatus = createBgBookingDto.bgbTotalPayStatus;
    bgBooking.bgbTotalPayMethod = createBgBookingDto.bgbTotalPayMethod;
    bgBooking.customer = customer;
    bgBooking.pet = pet;
    await this.bgBookingRepository.save(bgBooking);

    for (const so of createBgBookingDto.serviceOrders) {
      const serviceOrders = new ServiceOrder();
      const service = await this.serviceRepository.findOne({
        where: { serviceId: so.serviceId },
      });
      serviceOrders.serviceName = service.serviceName;
      serviceOrders.note = so.note;
      serviceOrders.service = service;
      serviceOrders.bgbooking = bgBooking;
      await this.serviceOrderRepository.save(serviceOrders);

      for (const soo of so.serviceOptionOrders) {
        const serviceOptionOrder = new ServiceOptionOrder();
        const option = await this.optionRepository.findOne({
          where: { optionId: soo.optionId },
        });
        serviceOptionOrder.optionName = option.optionName;
        // serviceOptionOrder.optionPrice = soo.optionPrice;
        serviceOptionOrder.option = option;
        serviceOptionOrder.serviceOrder = serviceOrders;
        await this.serviceOptionOrderRepository.save(serviceOptionOrder);
      }
    }
    await this.bgBookingRepository.save(bgBooking);
    return this.bgBookingRepository.findOne({
      where: {
        bgbId: bgBooking.bgbId,
      },
      relations: ['pet', 'customer', 'serviceOrders'],
    });
  }

  async findByCusId(cusId: number) {
    return await this.bgBookingRepository.find({
      where: { customer: { cusId } },
      order: { bgbId: 'DESC' },
      relations: ['serviceOrders'],
    });
  }

  findAll(query) {
    const search = query.search;
    return this.bgBookingRepository.find({
      where: [
        { bgbId: search },
        { customer: { cusName: Like(`%${search}%`) } },
        { customer: { cusSurname: Like(`%${search}%`) } },
      ],
      relations: {
        customer: true,
        pet: true,
        cageOrder: true,
        serviceOrders: { serviceOptionOrders: { option: true }, service: true },
      },
      order: { bgbId: 'DESC' },
    });
  }

  async findOne(bgbId: number) {
    const bgBooking = await this.bgBookingRepository.findOne({
      where: { bgbId: bgbId },
      relations: ['pet', 'customer'],
    });
    if (!bgBooking) {
      throw new NotFoundException();
    }
    return bgBooking;
  }

  async update(bgbId: number, updateBgBookingDto: UpdateBgBookingDto) {
    const bgBooking = await this.bgBookingRepository.findOne({
      where: { bgbId: bgbId },
    });
    if (!bgBooking) {
      throw new NotFoundException();
    }
    const updateBgBooking = Object.assign(bgBooking, updateBgBookingDto);
    return this.bgBookingRepository.save(updateBgBooking);
  }

  async remove(bgbId: number) {
    const bgbooking = await this.bgBookingRepository.findOne({
      where: { bgbId: bgbId },
      relations: ['serviceOrders', 'serviceOrders.serviceOptionOrders'],
    });

    if (!bgbooking) {
      throw new NotFoundException('BGbooking not found');
    }

    // ลบ ServiceOptionOrders
    for (const serviceOrder of bgbooking.serviceOrders) {
      await this.serviceOptionOrderRepository.remove(
        serviceOrder.serviceOptionOrders,
      );
    }

    // ลบ ServiceOrders และ CageOrders
    await this.serviceOrderRepository.remove(bgbooking.serviceOrders);
    // await this.cageOrderRepository.remove(bgbooking.cageOrder);

    // ลบ BGbooking
    await this.bgBookingRepository.remove(bgbooking);
  }
}
