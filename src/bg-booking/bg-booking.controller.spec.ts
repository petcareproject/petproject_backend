import { Test, TestingModule } from '@nestjs/testing';
import { BgBookingController } from './bg-booking.controller';
import { BgBookingService } from './bg-booking.service';

describe('BgBookingController', () => {
  let controller: BgBookingController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BgBookingController],
      providers: [BgBookingService],
    }).compile();

    controller = module.get<BgBookingController>(BgBookingController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
