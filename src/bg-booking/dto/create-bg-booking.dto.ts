import { IsNotEmpty, IsEmpty } from 'class-validator';
class CreateServiceOrderDto {
  @IsNotEmpty()
  serviceId: number;

  note: string;

  @IsNotEmpty()
  serviceOptionOrders: CreateServiceOptionOrderDto[];
}

class CreateServiceOptionOrderDto {
  @IsNotEmpty()
  optionId: number;
}

export class CreateBgBookingDto {
  @IsNotEmpty({ message: 'วันที่จอง' })
  bgbBookingDate: string;

  @IsNotEmpty({ message: 'เวลาที่จอง' })
  bgbBookingTime: string;

  @IsNotEmpty({ message: 'สถานะของการจอง' })
  bgbStatus: string;

  @IsNotEmpty({ message: 'ค่ามัดจํา' })
  bgbDepositPrice: number;

  @IsEmpty({ message: 'นํ้าหนักสัตว์เลี้ยง' })
  bgbPetWeight: number;

  @IsEmpty({ message: 'ค่าบริการ' })
  bgbServicePrice: number;

  @IsEmpty({ message: 'ราคารวม' })
  bgbTotalPrice: number;

  @IsNotEmpty({ message: 'สถานะค่ามัดจํา' })
  bgbDepPayStatus: string;

  @IsNotEmpty({ message: 'ช่องทางการชําระค่ามัดจํา' })
  bgbDepPayMethod: string;

  @IsEmpty({ message: 'สถานะค่าเงินจํานวนเต็ม' })
  bgbTotalPayStatus: string;

  @IsEmpty({ message: 'ช่องทางการชําระค่าเงินจํานวนเต็ม' })
  bgbTotalPayMethod: string;

  @IsNotEmpty()
  cusId: number;

  @IsNotEmpty()
  petId: number;

  @IsNotEmpty()
  serviceOrders: CreateServiceOrderDto[];
}
