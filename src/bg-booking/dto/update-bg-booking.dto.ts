import { PartialType } from '@nestjs/mapped-types';
import { CreateBgBookingDto } from './create-bg-booking.dto';

export class UpdateBgBookingDto extends PartialType(CreateBgBookingDto) {}
