import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { BgBookingService } from './bg-booking.service';
import { CreateBgBookingDto } from './dto/create-bg-booking.dto';
import { UpdateBgBookingDto } from './dto/update-bg-booking.dto';

@Controller('bgbooking')
export class BgBookingController {
  constructor(private readonly bgBookingService: BgBookingService) {}

  @Post()
  create(@Body() createBgBookingDto: CreateBgBookingDto) {
    return this.bgBookingService.create(createBgBookingDto);
  }

  @Get()
  findAll(@Query() query) {
    return this.bgBookingService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.bgBookingService.findOne(+id);
  }

  @Get('customer/:id')
  findByCustomer(@Param('id') id: string) {
    return this.bgBookingService.findByCusId(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateBgBookingDto: UpdateBgBookingDto,
  ) {
    return this.bgBookingService.update(+id, updateBgBookingDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.bgBookingService.remove(+id);
  }
}
