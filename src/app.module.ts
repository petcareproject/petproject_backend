import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PetsModule } from './pets/pets.module';
import { Pet } from './pets/entities/pet.entity';
import { Customer } from './customers/entities/customer.entity';
import { CustomersModule } from './customers/customers.module';
import { Service } from './services/entities/service.entity';
import { ServicesModule } from './services/services.module';
import { ServiceOrderModule } from './service-order/service-order.module';
import { ServiceOptionOrdersModule } from './service-option-orders/service-option-orders.module';
import { OptionsModule } from './options/options.module';
import { RoomsModule } from './rooms/rooms.module';
import { CagesModule } from './cages/cages.module';
import { CageOrdersModule } from './cage-orders/cage-orders.module';
import { Room } from './rooms/entities/room.entity';
import { Cage } from './cages/entities/cage.entity';
import { CageOrder } from './cage-orders/entities/cage-order.entity';
import { Option } from './options/entities/option.entity';
import { UsersModule } from './users/users.module';
import { ServiceOptionOrder } from './service-option-orders/entities/service-option-order.entity';
import { ServiceOrder } from './service-order/entities/service-order.entity';
import { User } from './users/entities/user.entity';
import { AuthModule } from './auth/auth.module';
import { BgBookingModule } from './bg-booking/bg-booking.module';
import { BGbooking } from './bg-booking/entities/bg-booking.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'petcare.db',
      entities: [
        Pet,
        Customer,
        Service,
        Option,
        Room,
        Cage,
        CageOrder,
        Service,
        Option,
        ServiceOptionOrder,
        ServiceOrder,
        User,
        BGbooking,
      ],
      synchronize: true,
    }),
    PetsModule,
    CustomersModule,
    ServicesModule,
    OptionsModule,
    RoomsModule,
    CagesModule,
    CageOrdersModule,
    UsersModule,
    ServiceOptionOrdersModule,
    ServiceOrderModule,
    AuthModule,
    BgBookingModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
