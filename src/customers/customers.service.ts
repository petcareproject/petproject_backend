import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
import { Customer } from './entities/customer.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class CustomersService {
  constructor(
    @InjectRepository(Customer)
    private customerRepository: Repository<Customer>,
  ) {}
  create(createCustomerDto: CreateCustomerDto) {
    return this.customerRepository.save(createCustomerDto);
  }

  findAll() {
    return this.customerRepository.find();
  }

  async findOneById(cusId: number) {
    const customer = await this.customerRepository.findOne({
      where: { cusId: cusId },
    });
    if (!customer) {
      throw new NotFoundException();
    }
    return customer;
  }
  findOneByEmail(email: string) {
    return this.customerRepository.findOne({ where: { cusEmail: email } });
  }

  async findOne(cusId: number) {
    const cus = await this.customerRepository.findOne({
      where: { cusId: cusId },
    });
    if (!cus) {
      throw new NotFoundException();
    }
    return cus;
  }

  async update(cusId: number, updateCustomerDto: UpdateCustomerDto) {
    const customer = await this.customerRepository.findOneBy({
      cusId: cusId,
    });
    if (!customer) {
      throw new NotFoundException();
    }
    const updateCustomer = Object.assign(customer, updateCustomerDto);
    return this.customerRepository.save(updateCustomer);
  }

  async remove(cusId: number) {
    const customer = await this.customerRepository.findOneBy({
      cusId: cusId,
    });
    if (!customer) {
      throw new NotFoundException();
    }
    return this.customerRepository.remove(customer);
  }
}
