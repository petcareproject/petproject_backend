import { Module } from '@nestjs/common';
import { CustomersService } from './customers.service';
import { CustomersController } from './customers.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Customer } from './entities/customer.entity';
import { Pet } from 'src/pets/entities/pet.entity';
import { BGbooking } from 'src/bg-booking/entities/bg-booking.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Customer, Pet, BGbooking])],
  controllers: [CustomersController],
  providers: [CustomersService],
  exports: [CustomersService],
})
export class CustomersModule {}
