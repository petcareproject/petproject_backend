import { IsNotEmpty } from 'class-validator';

export class CreateCustomerDto {
  cusPicture: string = 'no_image.jpg';

  @IsNotEmpty({ message: 'ชื่อผู้ใช้' })
  cusUsername: string;

  @IsNotEmpty({ message: 'รหัสผ่าน' })
  cusPassword: string;

  @IsNotEmpty({ message: 'อีเมล' })
  cusEmail: string;

  @IsNotEmpty({ message: 'ชื่อ' })
  cusName: string;

  @IsNotEmpty({ message: 'นามสกุล' })
  cusSurname: string;

  @IsNotEmpty({ message: 'เบอร์โทร' })
  cusPhone: string;
}
