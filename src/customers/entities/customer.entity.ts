import { BGbooking } from 'src/bg-booking/entities/bg-booking.entity';
import { Pet } from 'src/pets/entities/pet.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Customer {
  @PrimaryGeneratedColumn({ comment: 'รหัสลูกค้า' })
  cusId: number;

  @Column({ comment: 'รูปลูกค้า' })
  cusPicture: string;

  @Column({ comment: 'ชื่อผู้ใช้' })
  cusUsername: string;

  @Column({ comment: 'รหัสผ่าน' })
  cusPassword: string;

  @Column({ comment: 'อีเมล' })
  cusEmail: string;

  @Column({ comment: 'ชื่อ' })
  cusName: string;

  @Column({ comment: 'นามสกุล' })
  cusSurname: string;

  @Column({ comment: 'เบอร์โทร' })
  cusPhone: string;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @OneToMany(() => Pet, (pets) => pets.customer)
  pets: Pet[];

  @OneToMany(() => BGbooking, (bgbookings) => bgbookings.customer)
  bgbookings: BGbooking[];
}
