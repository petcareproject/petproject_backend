import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { CustomersService } from '../customers/customers.service';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class AuthService {
  constructor(
    private customersService: CustomersService,
    private userService: UsersService,
    private jwtService: JwtService,
  ) {}

  async signIn(email: string, pass: string): Promise<any> {
    const customer = await this.customersService.findOneByEmail(email);
    const user = await this.userService.findOneByEmail(email);

    if (!customer && !user) {
      throw new UnauthorizedException('ไม่พบข้อมูลผู้ใช้');
    }

    interface UserLogin {
      id: number;
      email: string;
      username: string;
      role: string;
      pic: string;
    }

    const userPayload: UserLogin = {
      id: 0,
      email: '',
      username: '',
      role: '',
      pic: '',
    };

    if (customer) {
      if (customer.cusPassword !== pass) {
        throw new UnauthorizedException('รหัสผ่านไม่ถูกต้อง');
      }
      userPayload.email = customer.cusEmail;
      userPayload.id = customer.cusId;
      userPayload.username = customer.cusUsername;
      userPayload.role = 'ลูกค้า';
      userPayload.pic = customer.cusPicture;
    }

    if (user) {
      if (user.userPassword !== pass) {
        throw new UnauthorizedException('รหัสผ่านไม่ถูกต้อง');
      }
      userPayload.email = user.userEmail;
      userPayload.id = user.userId;
      userPayload.username = user.userName;
      userPayload.role = user.userRole;
      userPayload.pic = user.userPicture;
    }

    const payload = {
      email: userPayload.email,
      id: userPayload.id,
      username: userPayload.username,
      role: userPayload.role,
      pic: userPayload.pic,
    };

    const userResult = user
      ? {
          id: user.userId,
          email: user.userEmail,
          username: user.userName,
          role: user.userRole,
          pic: user.userPicture,
        }
      : null;
    const customerResult = customer
      ? {
          id: customer.cusId,
          email: customer.cusEmail,
          username: customer.cusUsername,
          role: 'ลูกค้า',
          pic: customer.cusPicture,
        }
      : null;

    return {
      user: userResult,
      customer: customerResult,
      userPayload: userPayload,
      access_token: await this.jwtService.sign(payload),
    };
  }
}
