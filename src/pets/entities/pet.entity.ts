import { BGbooking } from 'src/bg-booking/entities/bg-booking.entity';
import { CageOrder } from 'src/cage-orders/entities/cage-order.entity';
import { Customer } from 'src/customers/entities/customer.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Pet {
  @PrimaryGeneratedColumn({ comment: 'รหัสสัตว์เลี้ยง' })
  petId: number;
  @Column({ comment: 'รูปสัตว์เลี้ยง' })
  petPicture: string;

  @Column({ comment: 'ชื่อสัตว์เลี้ยง' })
  petName: string;

  @Column({ comment: 'ชนิดสัตว์เลี้ยง' })
  petType: string;

  @Column({ comment: 'พันธุ์สัตว์เลี้ยง' })
  petBreed: string;

  @Column({ comment: 'วันเกิดสัตว์เลี้ยง' })
  petBirthday: Date;

  @Column({ comment: 'อายุสัตว์เลี้ยง' })
  petAge: string;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @ManyToOne(() => Customer, (customer) => customer.pets)
  @JoinColumn({ name: 'cusId', referencedColumnName: 'cusId' })
  customer: Customer;

  @OneToMany(() => BGbooking, (bgbooking) => bgbooking.pet)
  bgbookings: BGbooking[];

  @OneToMany(() => CageOrder, (cageOrders) => cageOrders.pet)
  cageOrders: CageOrder[];
}
