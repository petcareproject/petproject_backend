import { Module } from '@nestjs/common';
import { PetsService } from './pets.service';
import { PetsController } from './pets.controller';
import { Pet } from './entities/pet.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Customer } from 'src/customers/entities/customer.entity';
import { BGbooking } from 'src/bg-booking/entities/bg-booking.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Pet, Customer, BGbooking])],
  controllers: [PetsController],
  providers: [PetsService],
})
export class PetsModule {}
