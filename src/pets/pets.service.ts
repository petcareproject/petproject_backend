import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreatePetDto } from './dto/create-pet.dto';
import { UpdatePetDto } from './dto/update-pet.dto';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Pet } from './entities/pet.entity';
import { Customer } from 'src/customers/entities/customer.entity';

@Injectable()
export class PetsService {
  constructor(
    @InjectRepository(Pet)
    private petRepository: Repository<Pet>,
    @InjectRepository(Customer)
    private customerRepository: Repository<Customer>,
  ) {}
  async create(createPetDto: CreatePetDto) {
    const customer = await this.customerRepository.findOne({
      where: {
        cusId: createPetDto.cusId,
      },
    });
    console.log(createPetDto.petBirthday);
    const petBirthday = new Date(createPetDto.petBirthday);
    console.log(petBirthday);

    const pet = new Pet();
    pet.petPicture = createPetDto.petPicture;
    pet.petName = createPetDto.petName;
    pet.petType = createPetDto.petType;
    pet.petBreed = createPetDto.petBreed;
    pet.petBirthday = petBirthday;
    console.log(pet.petBirthday);
    pet.petAge = createPetDto.petAge;
    pet.customer = customer;
    await this.petRepository.save(pet);
    return this.petRepository.findOne({
      where: { petId: pet.petId },
      relations: ['customer'],
    });
  }

  async findByCusId(cusId: number) {
    return await this.petRepository.find({ where: { customer: { cusId } } });
  }

  async findAll() {
    return this.petRepository.find({
      relations: ['customer'],
    });
  }

  async findOne(petId: number) {
    const pet = await this.petRepository.findOne({
      where: { petId: petId },
      relations: ['customer'],
    });
    if (!pet) {
      throw new NotFoundException();
    }
    return pet;
  }

  async update(petId: number, updatePetDto: UpdatePetDto) {
    const pet = await this.petRepository.findOne({
      where: { petId: petId },
    });
    if (!pet) {
      throw new NotFoundException();
    }
    const updatePet = Object.assign(pet, updatePetDto);
    return this.petRepository.save(updatePet);
  }

  async remove(petId: number) {
    const pet = await this.petRepository.findOneBy({ petId: petId });
    if (!pet) {
      throw new ConflictException();
    }
    return this.petRepository.remove(pet);
  }
}
