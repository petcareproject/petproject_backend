import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
  Res,
  Patch,
} from '@nestjs/common';
import { PetsService } from './pets.service';
import { CreatePetDto } from './dto/create-pet.dto';
import { UpdatePetDto } from './dto/update-pet.dto';
import { diskStorage } from 'multer';
import { v4 as uuidv4 } from 'uuid';
import { extname } from 'path';
import { FileInterceptor } from '@nestjs/platform-express';
import { Response } from 'express';

@Controller('pets')
export class PetsController {
  constructor(private readonly petsService: PetsService) {}
  @UseInterceptors(
    FileInterceptor('petPicture', {
      storage: diskStorage({
        destination: './pet_images',
        filename: (req, file, cb) => {
          const name = uuidv4();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  @Post()
  create(
    @Body() createPetDto: CreatePetDto,
    @UploadedFile() petPicture: Express.Multer.File,
  ) {
    console.log(petPicture.filename);
    createPetDto.petPicture = petPicture.filename;
    return this.petsService.create(createPetDto);
  }

  @Get()
  findAll() {
    return this.petsService.findAll();
  }

  @Get('customer/:id')
  findByCustomer(@Param('id') id: string) {
    return this.petsService.findByCusId(+id);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.petsService.findOne(+id);
  }

  @Get(':id/images')
  async getImage(@Param('id') id: string, @Res() res: Response) {
    const pet = await this.petsService.findOne(+id);
    res.sendFile(pet.petPicture, { root: './pet_images' });
  }

  @Get('image/:imageFile')
  async getImageByFileName(
    @Param('imageFile') imageFile: string,
    @Res() res: Response,
  ) {
    res.sendFile(imageFile, { root: './pet_images' });
  }

  @Patch(':id')
  @UseInterceptors(
    FileInterceptor('petPicture', {
      storage: diskStorage({
        destination: './pet_images',
        filename: (req, file, cb) => {
          const name = uuidv4();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  update(
    @Param('id') id: string,
    @Body() updatePetDto: UpdatePetDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) {
      updatePetDto.petPicture = file.filename;
    }
    return this.petsService.update(+id, updatePetDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.petsService.remove(+id);
  }
}
