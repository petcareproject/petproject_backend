import { IsDateString, IsNotEmpty, MinLength } from 'class-validator';

export class CreatePetDto {
  petPicture: string = 'no_image.jpg';

  @IsNotEmpty()
  @MinLength(3)
  petName: string;

  @IsNotEmpty()
  petType: string;

  @IsNotEmpty()
  petBreed: string;

  @IsDateString()
  @IsNotEmpty()
  petBirthday: Date;

  @MinLength(1)
  petAge: string;

  @IsNotEmpty()
  cusId: number;
}
