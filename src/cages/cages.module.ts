import { Module } from '@nestjs/common';
import { CagesService } from './cages.service';
import { CagesController } from './cages.controller';
import { Cage } from './entities/cage.entity';
import { Room } from 'src/rooms/entities/room.entity';
import { CageOrder } from 'src/cage-orders/entities/cage-order.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Cage, Room, CageOrder])],
  controllers: [CagesController],
  providers: [CagesService],
})
export class CagesModule {}
