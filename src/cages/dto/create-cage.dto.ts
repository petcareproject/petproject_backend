import { IsNotEmpty } from 'class-validator';

export class CreateCageDto {
  @IsNotEmpty()
  cageStatus: string;

  @IsNotEmpty()
  roomId: number;
}
