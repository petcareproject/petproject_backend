import { CageOrder } from 'src/cage-orders/entities/cage-order.entity';
import { Room } from 'src/rooms/entities/room.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Cage {
  @PrimaryGeneratedColumn({ comment: 'รหัสกรง' })
  cageId: number;

  @Column({ comment: 'สถานะกรง' })
  cageStatus: string;

  @ManyToOne(() => Room, (room) => room.cages)
  @JoinColumn({ name: 'roomId', referencedColumnName: 'roomId' })
  room: Room;

  @OneToMany(() => CageOrder, (cageOrders) => cageOrders.cage)
  cageOrders: CageOrder[];

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;
}
