import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateCageDto } from './dto/create-cage.dto';
import { UpdateCageDto } from './dto/update-cage.dto';
import { Cage } from './entities/cage.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Room } from 'src/rooms/entities/room.entity';

@Injectable()
export class CagesService {
  constructor(
    @InjectRepository(Cage)
    private cageRepository: Repository<Cage>,
    @InjectRepository(Room)
    private roomRepository: Repository<Room>,
  ) {}
  async create(createCgaeDto: CreateCageDto) {
    const room = await this.roomRepository.findOne({
      where: {
        roomId: createCgaeDto.roomId,
      },
    });

    const cage = new Cage();
    cage.cageStatus = createCgaeDto.cageStatus;
    cage.room = room;
    await this.cageRepository.save(cage);
    return this.cageRepository.findOne({
      where: { cageId: cage.cageId },
      relations: ['room'],
    });
  }

  async findAll() {
    return this.cageRepository.find({
      relations: ['room'],
    });
  }

  async findOne(cageId: number) {
    const cage = await this.cageRepository.findOne({
      where: { cageId: cageId },
      relations: ['room'],
    });
    if (!cage) {
      throw new NotFoundException();
    }
    return cage;
  }

  async findByRoomId(roomId: number) {
    return await this.cageRepository.find({ where: { room: { roomId } } });
  }

  async update(cageId: number, updateCageDto: UpdateCageDto) {
    const cage = await this.cageRepository.findOne({
      where: { cageId: cageId },
    });
    if (!cage) {
      throw new NotFoundException();
    }
    const updateCage = Object.assign(cage, updateCageDto);
    return this.cageRepository.save(updateCage);
  }

  async remove(cageId: number) {
    const cage = await this.cageRepository.findOne({
      where: { cageId: cageId },
    });
    if (!cage) {
      throw new ConflictException();
    }
    return this.cageRepository.remove(cage);
  }
}
