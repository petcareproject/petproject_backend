import { IsNotEmpty, MinLength } from 'class-validator';

export class CreateUserDto {
  userPicture: string = 'no_image.jpg';

  @IsNotEmpty({ message: 'ชื่อผู้ใช้' })
  @MinLength(4, { message: 'ขั้นต่ำของ username คือ 4 ตัวอักษร' })
  userName: string;

  @IsNotEmpty({ message: 'รหัสผู้ใช้' })
  userPassword: string;

  @IsNotEmpty({ message: 'ชื่อจริงผู้ใช้' })
  userFirstname: string;

  @IsNotEmpty({ message: 'นามสกุลผู้ใช้' })
  userLastname: string;

  @IsNotEmpty({ message: 'อีเมลผู้ใช้' })
  userEmail: string;

  @IsNotEmpty({ message: 'ตําเเหน่งผู้ใช้' })
  userRole: string;
}
