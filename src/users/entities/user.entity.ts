import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn({ comment: 'รหัสผู้ใช้งาน' })
  userId: number;

  @Column({ comment: 'รูปผู้ใช้งาน' })
  userPicture: string;

  @Column({ comment: 'ชื่อผู้ใช้งาน' })
  userName: string;

  @Column({ comment: 'รหัสผู้ใช้งาน' })
  userPassword: string;

  @Column({ comment: 'ชื่อผู้ใช้งาน' })
  userFirstname: string;

  @Column({ comment: 'นามสกุลผู้ใช้งาน' })
  userLastname: string;

  @Column({ comment: 'อีเมลผู้ใช้งาน' })
  userEmail: string;

  @CreateDateColumn({ comment: 'วันเริ่มงาน' })
  workStartDate: string;

  @Column({ comment: 'ตําเเหน่งผู้ใช้งาน' })
  userRole: string;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;
}
